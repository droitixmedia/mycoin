<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

public function run()
{
    $users = factory(openjobs\User::class, 1002)->make()->toArray();

    foreach ($users as $user) {
        openjobs\User::create($user);
    }
}




  }  