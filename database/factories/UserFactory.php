<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(openjobs\User::class, function (Faker $faker) {
    return [
         'password' => bcrypt('secret'),
        'name' => $faker->name,
        'surname' => 'zambia',
        'phone' => '0612384885',
        'bank' => 'airtel',
        'account' => '0612344885',
        'branch' => '23623',
        'referred_by' => '1',
        'accounttype' => 'max',
        'btcaddress' => 'xtretdg',

        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
      
        'remember_token' => str_random(10),
    ];
});
