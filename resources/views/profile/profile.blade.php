@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
     @if (session()->has('impersonate'))
                                    
<div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="page-header"></div>
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="row">
                     <div class="col-sm-12">
                        <h5 class="mb-3">ADMIN UPDATE</h5>
                        <hr>
                        <div class="accordion" id="accordionExample">
                           <form action="{{route('profile.update')}}" method="POST">
                            @csrf
                              <div class="card mb-4 py-3 border-left-primary">
                                 <div class="card-body">
                                    <p></p>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Payment Status</label>
                                       
                                       <input type="text" class="custom-select"  name="paid" value="{{$user->paid}}">
                                       
                                    </div>
                                
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Mobile Number</label>
                                       <input type="text" class="custom-select" name="banking_status" value="disabled" hidden="">
                                       <input type="text" class="custom-select" placeholder="WhatsApp Number" name="phone" value="{{$user->phone}}">
                                       <small id="emailHelp" class="form-text text-muted">We'll never share this, its ony for all communications to you.</small>
                                    </div>
                                    <hr>
                                   
                                    <hr>
                    
                                    <div id="cont" style="display: block;">
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Bank Name</label>
                                           <input type="text" class="custom-select" placeholder="Bank" name="bank" value="{{$user->bank}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Account Holder Name</label>
                                          <input type="text" class="custom-select" placeholder="Account Name" name="name" value="{{$user->name}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Account Number</label>
                                          <input type="text" class="custom-select" placeholder="Account Number" name="account" value="{{$user->account}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       
                                       <hr>
                                     
                                       <hr>
                                    </div>
                                   
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Notes To Display To The Buyer </label>
                                       <textarea cols="30" rows="10" class="form-control" value="{{$user->branch}}" name="branch">{{$user->branch}}</textarea>
                                       <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                    </div>
                                    <input type="hidden" class="form-control" name="btcaddress" value="htfrt">
                                    <input type="hidden" class="form-control" name="surname" value="SA">
                                     <input type="hidden" class="form-control" name="email" value="{{$user->email}}">
                                     <input type="hidden" class="form-control" name="accounttype" value="SA">

                                    <hr>
                                    <button type="submit" class="btn btn-primary btn-icon-split">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-check"></i>
                                    </span>
                                    <span class="text">Save</span>
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


                                     @else

<div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="page-header"></div>
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="row">
                     <div class="col-sm-12">
                        <h5 class="mb-3">Banking Details</h5>
                        <hr>
                        <div class="accordion" id="accordionExample">
                           <form action="{{route('profile.update')}}" method="POST">
                            @csrf
                              <div class="card mb-4 py-3 border-left-primary">
                                 <div class="card-body">
                                    <p></p>
                                
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Mobile Number</label>
                                       <input type="text" class="custom-select" name="banking_status" value="disabled" hidden="">
                                       <input type="text" class="custom-select" placeholder="WhatsApp Number" name="phone" value="{{$user->phone}}">
                                       <small id="emailHelp" class="form-text text-muted">We'll never share this, its ony for all communications to you.</small>
                                    </div>
                                    <hr>
                                   
                                    <hr>
                    
                                    <div id="cont" style="display: block;">
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Bank Name</label>
                                           <input type="text" class="custom-select" placeholder="Bank" name="bank" value="{{$user->bank}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Account Holder Name</label>
                                          <input type="text" class="custom-select" placeholder="Account Name" name="name" value="{{$user->name}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       <div class="form-group">
                                          <label for="exampleInputEmail1">Account Number</label>
                                          <input type="text" class="custom-select" placeholder="Account Number" name="account" value="{{$user->account}}">
                                          <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                       </div>
                                       <hr>
                                       
                                       <hr>
                                     
                                       <hr>
                                    </div>
                                    <input type="hidden" name="paid" value="{{$user->paid}}">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Notes To Display To The Buyer </label>
                                       <textarea cols="30" rows="10" class="form-control" value="{{$user->branch}}" name="branch">{{$user->branch}}</textarea>
                                       <small id="emailHelp" class="form-text text-muted">We'll show this to buyers making payments to you.</small>
                                    </div>
                                    <input type="hidden" class="form-control" name="btcaddress" value="htfrt">
                                    <input type="hidden" class="form-control" name="surname" value="SA">
                                     <input type="hidden" class="form-control" name="email" value="{{$user->email}}">
                                     <input type="hidden" class="form-control" name="accounttype" value="SA">

                                    <hr>
                                    <button type="submit" class="btn btn-primary btn-icon-split">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-check"></i>
                                    </span>
                                    <span class="text">Save</span>
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endif

@endsection
