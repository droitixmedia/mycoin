@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="pcoded-main-container">
<div class="pcoded-wrapper">
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="page-header">
</div>

<div class="main-body">
<div class="page-wrapper">

<div class="row">
<div class="col-sm-12">
<h5 class="mb-3">Account Password</h5>
<hr>
<div class="accordion" id="accordionExample">
<form method="POST" action="{{ route('change.password') }}" role="form" class="forms-sample">
    @csrf

    @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach

<div class="card mb-4 py-3 border-left-primary">
<div class="card-body">
     <p></p>

<input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password" placeholder="Current Password">
<p></p>

<input class="custom-select" id="new_password" type="password" class="form-control" name="new_password" required placeholder="New Password">
<p></p>

<input class="custom-select" id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" required placeholder="New Password">
<p></p>
<button type="submit" class="btn btn-success btn-icon-split">
<span class="icon text-white-50">
<i class="fas fa-check"></i>
</span>
<span class="text">Save</span>
</button>
</div>
</div>
</form>
</div>
</div>

</div>

</div>
</div>
</div>
</div>
</div>
</div>


@endsection
