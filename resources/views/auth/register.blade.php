<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codedthemes.com/demos/admin-templates/datta-able/bootstrap/default/auth-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Jan 2022 00:43:25 GMT -->
<head>
<title>My Coin Auction-2023</title>


<!--[if lt IE 10]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="The Ultimate Auction" />
<meta name="keywords" content="" />
<meta name="author" content="CodedThemes" />

<link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="/assets/fonts/fontawesome/css/fontawesome-all.min.css">

<link rel="stylesheet" href="/assets/plugins/animation/css/animate.min.css">

<link rel="stylesheet" href="/assets/css/style.css">
</head>
<body>
<div class="auth-wrapper">
<div class="auth-content">
<div class="auth-bg">
<span class="r"></span>
<span class="r s"></span>
<span class="r s"></span>
<span class="r"></span>
</div>
<div class="card">
<div class="card-body text-center">
<div class="mb-4">
<i class="feather icon-user-plus auth-icon"></i>
</div>
<h3 class="mb-4">Sign up</h3>
 <form action="{{ route('register') }}" method="POST" autocomplete="off">
                                    @csrf
<div class="input-group mb-3">
<input type="text" name="name" class="form-control" placeholder="Username">
</div>
<div class="input-group mb-3">
<input type="email" name="email" class="form-control" placeholder="Email">
</div>
<div class="input-group mb-3">
<input type="text" name="phone" class="form-control" placeholder="Phone Number">
</div>

<div class="input-group mb-3">
<input type="text" name="referred_by" class="form-control" placeholder="Sponsor ID">
</div>
<div class="input-group mb-4">
<input type="password" class="form-control" name="password" placeholder="password">
</div>

<div class="input-group mb-4">
<input type="password" class="form-control" name="password_confirmation" placeholder="Type password again">
</div>
<input type="hidden" name="surname"   value="0" >
<input type="hidden" name="bank"   value="0" >
<input type="hidden" name="account"   value="0" >
<div class="form-group text-left">
<div class="checkbox checkbox-fill d-inline">
<input type="checkbox" name="checkbox-fill-2" id="checkbox-fill-2">
<label for="checkbox-fill-2" class="cr">Save Details <a href="#!"> </label>
</div>
</div>
<button type="submit" class="btn btn-primary shadow-2 mb-4">Sign up</button>
</form>
<p class="mb-0 text-muted">Already have an account? <a href="{{url('login')}}"> Log in</a></p>
</div>
</div>
</div>
</div>

<script src="/assets/js/vendor-all.min.js"></script><script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/pcoded.min.js"></script>

</body>

<!-- Mirrored from codedthemes.com/demos/admin-templates/datta-able/bootstrap/default/auth-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Jan 2022 00:43:25 GMT -->
</html>
