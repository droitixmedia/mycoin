@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


 <div class="pcoded-main-container">
         <div class="pcoded-wrapper">
            <div class="pcoded-content">
               <div class="pcoded-inner-content">
                  <div class="page-header">
                     <div class="page-block">
                        <div class="row align-items-center">
                           <div class="col-md-12">
                              <div class="page-header-title">
                                 <h5 class="m-b-10">Welcome to My Coin Auction</h5>
                              </div>
                              <ul class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="feather icon-home"></i></a></li>
                                 <li class="breadcrumb-item"><a href="#!">home of {{Auth::user()->name}}</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="main-body">
                     <div class="page-wrapper">
                        <div class="row">
                           <div class="col-md-6 col-xl-4">
                              <div class="card Online-Order">
                                 <div class="card-block">
                                    <h5>Bids Income</h5>
                                    <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">R 0<span class="float-right f-18 text-c-green">0 Coins</span></h6>
                                    <div class="progress mt-3">
                                       <div class="progress-bar progress-c-theme" role="progressbar" style="width:100%;height:6px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                   
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6 col-xl-4">
                              <div class="card Online-Order">
                                 <div class="card-block">
                                    <h5>Bonus Income</h5>
                                    <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">R 0<span class="float-right f-18 text-c-purple">0 Coins</span></h6>
                                    <div class="progress mt-3">
                                       <div class="progress-bar progress-c-theme2" role="progressbar" style="width:100%;height:6px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12 col-xl-4">
                              <div class="card Online-Order">
                                 <div class="card-block">
                                    <h5>Sponsor ID</h5>
                                    <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">{{Auth::user()->id}}<span class="float-right f-18 text-c-blue">For Bonus</span></h6>
                                    <div class="progress mt-3">
                                       <div class="progress-bar progress-c-blue" role="progressbar" style="width:100%;height:6px;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                   
                                 </div>
                              </div>
                           </div>
                        
                          
                           <div class="col-xl-8 col-md-6">
                              <div class="card code-table">
                                 <div class="card-header">
                                    <h5>Recent Bids</h5>
                                    <div class="card-header-right">
                                       <div class="btn-group card-option">
                                          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="feather icon-more-horizontal"></i>
                                          </button>
                                          <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                             <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                             <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                             <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                             <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-block pb-0">
                                    <div class="table-responsive">
                                       <table class="table table-hover">
                                          <thead>
                                             <tr>
                                                
                                                <th>Bid</th>
                                                <th>Status</th>
                                                <th>Auction Status</th>
                                                
                                               
                                          </thead>
                                           @if ($comments->count())
        @each ('listings.partials._comment_own', $comments, 'comment')
        {{ $comments->links() }}
    @else
       
    @endif

                                         
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-xl-4 col-md-6">
                                        <div class="card card-event">
                                            <div class="card-block">
                                                <div class="row align-items-center justify-content-center">
                                                    <div class="col">
                                                        <h5 class="m-0">Next Auction Entries</h5>
                                                    </div>
                                                </div>
                                                     @php ($sum = 0)

                 @foreach($listings as $listing)

                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif



                     @endforeach

                     @php ($diff = 0)

                      @foreach($listings as $listing)


                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                      @endforeach

                                                <h2 class="mt-3 f-w-300">
                                                    
                                                 R {{number_format($sum-$diff)}}                                                 <sub class="text-muted f-14">Coins</sub>
                                                </h2>
                                                <h6 class="text-muted mt-4 mb-0">You can participate in event </h6>
                                                <h6 class="text-muted mt-4 mb-0">You have
                                                    0 coins on sale
                                                </h6>
                                                <h6 class="text-muted mt-4 mb-0">


                                                    
                                                </h6>
                                                <i class="fas fa-gavel text-c-purple f-50"></i>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-block border-bottom">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto">
                                                        <i class="feather icon-zap f-30 text-c-green"></i>
                                                    </div>
                                                    <div class="col">
                                                        <h3 class="f-w-300 at" id="demo">9AM</h3>
                                                        <span class="d-block text-uppercase">Next Auction</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto">
                                                        <i class="feather icon-map-pin f-30 text-c-blue"></i>
                                                    </div>
                                                    <div class="col">
                                                        <h3 class="f-w-300">Daily Auction Times</h3>
                                                        <span class="d-block text-uppercase">
                                                            09:00AM
                                                        </span>
                                                        <span class="d-block text-uppercase">
                                                            19:00PM
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto">
                                                        <i class="feather icon-link-2 f-30 text-c-blue"></i>
                                                    </div>
                                                    <div class="col">
                                                        <h3 class="f-w-300">Social Groups</h3>
                                                        <span class="d-block text-uppercase"><a href="https://t.me/OriginalMyCoinAuction_MCA" target="_blank">Join
                                                                Telegram</a></span>
                                                        <span class="d-block text-uppercase"><a href="https://chat.whatsapp.com/LN3zs6suPSs7dnVHf9xE9M" target="_blank">Join
                                                                WhatsApp</a></span>
                                                        <span class="d-block text-uppercase"><a href="https://www.facebook.com/MyCoinAuction" target="_blank">Join
                                                                Facebook</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

 



@endsection
