@extends('layouts.app')
@section('title', 'Dashboard')
@section('description')
@endsection
@section('content')
<div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="page-header">
            </div>
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="row">
                     <div class="col-sm-12">
                        <h5 class="mb-3">Pay Bids</h5>
                        Please use correct REFERENCE as indicated when paying, after paying PLEASE
                        attach PROOF OF PAYMENT
                        <hr>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <h5 style="color: #1c7aa5;">Payment of R{{$comment->split}}</h5>
                        <hr>
                        <h5>Bid Number: </h5>
                        <h5 style="color: red;">2022{{$comment->id}}</h5>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">POP</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Chat</a>
                           </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                              <h6>PAYMENT DUE DETAILS</h6>
                              <br>
                              <p class="mb-0">
                              <h6>COUNTRY: SA</h6>
                              <h6>MOBILE MONEY: {{$comment->listing->user->bank}}</h6>
                              <h6>MOBILE MONEY NUMBER: {{$comment->listing->user->account}}</h6>
                              <h6>ACCOUNT HOLDER: {{$comment->listing->user->name}} {{$comment->listing->user->surname}}</h6>
                              <hr>
                              <h6>CONTACT: {{$comment->listing->user->phone}}</h6>
                              <hr>
                              <h6>USER NOTES: You can call for approval</h6>
                              </p>
                           </div>
                           <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <h5>PROOF OF PAYMENT</h5>
                              <p class="mb-0">   
                              <form action="/uploaddocs" method="post" enctype="multipart/form-data">
                                 {{ csrf_field() }}
                                 <div class="form-group">
                                    <input type="hidden" name="name" value="0" class="form-control">
                                 </div>
                                 <br />
                                 <input type="file" class="form-control" name="cvs[]" multiple />
                                 <br />
                                 <input type="submit" class="btn  btn-success" value="Upload" />
                              </form>
                              </p>
                           </div>
                           <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                              <p class="mb-0"></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection