@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Upload Payment Proof</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                 <div class="row">
                    <div class="col-md-6">
                         <div class="card mb-5">
                            <div class="card-body">

                                      <form action="/uploaddocs" method="post" enctype="multipart/form-data">
{{ csrf_field() }}

                                <div class="form-group">

                                            <input type="hidden" name="name" value="0" class="form-control">

                                        </div>


<br />
<input type="file" class="form-control" name="cvs[]" multiple />
<br />
<input type="submit" class="btn  btn-success" value="Upload Proof" />

</form>
                            </div>
                        </div>

                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
