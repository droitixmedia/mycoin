@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
<div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">

                        <div class="page-header">
                        </div>

                        <div class="main-body">
                            <div class="page-wrapper">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="mb-3">Pay Bids</h5>
     Please use correct REFERENCE as indicated when paying, after paying PLEASE
                                        attach PROOF OF PAYMENT
                                        <hr>

                         @if ($comments->count())
        @each ('listings.partials.comment_own', $comments, 'comment')
        {{ $comments->links() }}
    @else
       
    @endif

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

@endsection
