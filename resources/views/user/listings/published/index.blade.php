@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">

                        <div class="page-header">
                        </div>

                        <div class="main-body">
                            <div class="page-wrapper">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="mb-3">Recieve Bids</h5>
                                        <hr>
                                         
                                    @if ($listings->count())
        @each ('listings.partials.listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">Nothing to recieve</p></th>
    @endif                                   </div>
                                </div>
                                <div class="row">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection
