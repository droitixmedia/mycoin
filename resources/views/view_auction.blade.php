@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="page-header">
            </div>
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="col-xl-12">
                     <div class="row">


                        <div class="col-sm-12 col-md-3">
                           <div class="card text-left">
                              <div class="card-body">
                                 <h5 class="card-title">What is your BID range amount?</h5>
                                 <hr>
                                 <h5 class="card-title">K100-K1000</h5>
                                 <a href="{{url('exchange0')}}" class="btn btn-secondary">Bid Here</a>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-12 col-md-3">
                           <div class="card text-left">
                              <div class="card-body">
                                 <h5 class="card-title">What is your BID range amount?</h5>
                                 <hr>
                                 <h5 class="card-title">K1000-K5000</h5>
                                 <a href="{{url('exchange1')}}" class="btn btn-secondary">Bid Here</a>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-12 col-md-3">
                           <div class="card text-left">
                              <div class="card-body">
                                 <h5 class="card-title">What is your BID range amount?</h5>
                                 <hr>
                                 <h5 class="card-title">K5000-K10000</h5>
                                 <a href="{{url('exchange2')}}" class="btn btn-secondary">Bid Here</a>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-12 col-md-3">
                           <div class="card text-left">
                              <div class="card-body">
                                 <h5 class="card-title">What is your BID range amount?</h5>
                                 <hr>
                                 <h5 class="card-title">K10000-K100 000</h5>
                                 <a href="{{url('exchange3')}}" class="btn btn-secondary">Bid Here</a>
                              </div>
                           </div>
                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection