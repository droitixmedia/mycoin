
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

@if ($time >= $morningstart && $time <= $morningend)


<div class="col-xl-12">
<div class="row">
<div class="col-sm-12 col-md-4">
<h5>Left Align</h5>
<hr>
<div class="card text-left">
<div class="card-body">
<h5 class="card-title">Special title treatment</h5>
<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
<a href="#!" class="btn btn-primary">Go somewhere</a>
</div>
</div>
</div>
<div class="col-sm-12 col-md-4">
<h5>Center Align</h5>
<hr>
<div class="card text-center">
<div class="card-body">
<h5 class="card-title">Special title treatment</h5>
<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
<a href="#!" class="btn btn-primary">Go somewhere</a>
</div>
</div>
</div>
<div class="col-sm-12 col-md-4">
<h5>Right Align</h5>
<hr>
<div class="card text-right">
<div class="card-body">
<h5 class="card-title">Special title treatment</h5>
<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
<a href="#!" class="btn btn-primary">Go somewhere</a>
</div>
</div>
</div>
</div>
</div>


@else



@endif


@endsection