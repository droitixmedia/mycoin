@extends('layouts.app')
@section('title', 'Dashboard')
@section('description')
@endsection
@section('content')
@if ($time >= $morningstart && $time <= $morningend)
<div class="pcoded-main-container">
<div class="pcoded-wrapper">
   <div class="pcoded-content">
      <div class="pcoded-inner-content">
         <div class="page-header">
         </div>
         <div class="main-body">
            <div class="page-wrapper">
               <div class="col-xl-12">
                  <div class="row">
                     @foreach ($listings as $listing)
                     @php ($sum = 0)
                     @php ($limit = 5000)
                     @foreach($listing->comments as $comment)
                     @php ($sum += $comment->split)
                     @if ($loop->last)
                     @endif
                     @endforeach
                     
                     @if ($listing->amount > $sum)
                       @if ($listing->amount-$sum < $limit)


                       @else

                     <div class="col-sm-12 col-md-3">
                        <div class="card text-left">
                           <div class="card-body">
                              <h5 class="card-title">Lot #2022{{$listing->id}}</h5>
                              <hr>
                              <h5 class="card-title">{{$listing->user->bank}}</h5>
                              <h3 class="card-title">{{$listing->amount - $sum}}.00 Coins</h3>
                              <h6 class="card-title"> Going Price is K{{$listing->amount - $sum}}</h6>
                              @if(session('success'))
                              <h1 style="color:green">{{session('success')}}</h1>
                              @endif
                              @if($listing->amount-$sum < 300)
                              <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                                 @csrf
                                 <select name="category_id" class="mb-3 form-control">
                                    <option type="disabled" value="5">Select Bid Period</option>
                                    <option value="5">5 Days</option>
                                    <option value="10">10 Days</option>
                                    <option value="15">15 Days</option>
                                 </select>
                                 <div class="form-group">
                                    <input type="hidden" class="form-control" name="split" id="split" value="{{$listing->amount-$sum}}">
                                 </div>
                                 <input type="hidden" class="form-control" name="body" id="body" value="{{$listing->amount-$sum}}">
                                 <button class="btn btn-primary" type="submit">Bid All</button>
                              </form>
                              @else
                              <form action="{{ route('comments.store', [$listing->id]) }}" method="post">
                                 @csrf
                                 <select name="category_id" class="mb-3 form-control">
                                    <option type="disabled" value="5">Select Bid Period</option>
                                    <option value="5">5 Days</option>
                                    <option value="10">10 Days</option>
                                    <option value="15">15 Days</option>
                                 </select>
                                 <div class="form-group">
                                    <div class="form-group{{ $errors->has('split') ? ' has-error' : '' }}">
                                       <input type="text" name="split"  class="form-control" placeholder="500">
                                    </div>
                                    <input type="hidden" class="form-control" name="body" id="body" value="{{$listing->amount-$sum}}">
                                    <button class="btn btn-primary" type="submit">Bid </button>
                              </form>
                              @endif
                              @endif


                              </div>
                           </div>
                        </div>
                       

                       
                     </div>
                      @else


                        @endif
 @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@else
<div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="page-header">
            </div>
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="row">
                     <div class="col-xl-6 col-md-6">
                        <div class="card card-event">
                           <div class="card-block">
                              <div class="row align-items-center justify-content-center">
                                 <div class="col">
                                    <h5 class="m-0"></h5>
                                 </div>
                              </div>
                              <h2 class="mt-3 f-w-300">
                                                                             <sub class="text-muted f-14">Coins</sub>
                              </h2>
                              <h6 class="text-muted mt-4 mb-0">You can participate in event </h6>
                              <i class="fab fa-angellist text-c-purple f-50"></i>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-6 col-md-6">
                        <div class="card">
                           <div class="card-block border-bottom">
                              <div class="row d-flex align-items-center">
                                 <div class="col-auto">
                                    <i class="feather icon-zap f-30 text-c-green"></i>
                                 </div>
                                 <div class="col">
                                    <h3 class="f-w-300 at " id="demo"></h3>
                                    <span class="d-block text-uppercase">Auction Not Started Yet</span>
                                 </div>
                              </div>
                           </div>
                           <div class="card-block">
                              <div class="row d-flex align-items-center">
                                 <div class="col-auto">
                                    <i class="feather icon-map-pin f-30 text-c-blue"></i>
                                 </div>
                                 <div class="col">
                                    <h3 class="f-w-300">Daily Auction Times</h3>
                                    <span class="d-block text-uppercase">
                                    08:30  Hrs
                                    </span>
                                    <span class="d-block text-uppercase">
                                    18:30  Hrs
                                    </span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endif
@endsection