@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="pcoded-main-container">
<div class="pcoded-wrapper">
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="page-header">
<div class="page-block">
<div class="row align-items-center">
</div>
</div>
</div>

<div class="main-body">
<div class="page-wrapper">

<div class="row">


<div class=" col-md-12">
<div class="card card-event">
<div class="card-block">
<div class="row align-items-center justify-content-center">
<div class="col">
<h5 class="m-0">System Notifications</h5>
</div>
</div>

 <h3 class="mt-3 f-w-300"><b><span style="color:#A389D4">Welcome to the My Coin Auction</span></b><sub class="text-muted f-14"></sub></h3>
<p> </p><p>
<b>BY popular demand MCA is back!!! </b></p><p>



 </p><h3 class="mt-3 f-w-300"><b><span style="color:#A389D4">IMPORTANT NOTICE </span> <span class="text-danger">‼</span></b><sub class="text-muted f-14"></sub></h3>
<p> </p><p>
<b>PLEASE NOTE FROM TODAY BUYERS NEEDS TO ATTACH POP IN THE SYSTEM FOR THEM TO BE APPROVED, SELLERS WILL NOT BE ABLE TO APPROVE YOU IF NO POP ATTACHED.
<br><br>
 BOTH PDF AND IMAGE WILL BE ACCEPTED, SHOULD YOU FIND DIFFICULTIES IN UPLOADING POPS IN THE SYSTEM PLEASE INBOX ADMINS THEY WILL GUIDE YOU 
<br><br>
MEMBERS ARE TO MAKE PAYMENTS TO THE DETAILS IN THE SYSTEM ONLY <span class="text-danger">‼</span> DO NOT TAKE ANY OTHER PAYMENT DETAILS FROM INBOXES, CALLS OR EVEN YOUR BUYER. STRICTLY USE DETAILS ON THE SYSTEM <br>
OTHER PAYMENTS ARRANGEMENTS SHOULD BE MADE THROUGH MAIN ADMIN @ MCA ADMIN ON WHATSAPP.
<br><br> 
MCA WILL NOT BE LIABLE SHOULD YOU FALL VICTIM OF SCAMMERS BY TAKING PAYMENTS DETAILS FROM THE INBOXES! DO NOT SHARE YOUR LOGIN DETAILS WITH RANDOM PEOPLE 
<br><br>
MCA ADMINS WILL NOT CONTACT /TEXT YOU ON TELEGRAM<br><br>

<span class="text-success">HAPPY EARNING WITH MCA
<br><br>
MCA MANAGEMENT</span></b></p><p>



 
</p><h6 class="text-muted mt-4 mb-0"><a href="{{url('/dash')}}" class="btn btn-secondary">Got It</a></h6>
<hr>
</div>
</div>
</div>

</div>

</div>
</div>
</div>
</div>
</div>
</div>



@endsection
