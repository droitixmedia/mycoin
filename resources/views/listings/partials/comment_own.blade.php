
                                        <div class="accordion" id="accordionExample">
<div class="card">
<div class="card-header" id="headingOne">
<h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">Payment of R{{$comment->split}}</a></h5>
</div>
<div id="collapseOne" class="card-body collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style=""><h5>Bid Number: </h5>
                        <h5 style="color: red;">2022{{$comment->id}}</h5>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Details</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">POP</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link text-uppercase show" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Chat</a>
                           </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                           <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                              <h6>PAYMENT DUE DETAILS</h6>
                              <br>
                              <p class="mb-0">
                              <h6>COUNTRY: SA</h6>
                              <h6>BANK: {{$comment->listing->user->bank}}</h6>
                              <h6>ACCOUNT NUMBER: {{$comment->listing->user->account}}</h6>
                              <h6>ACCOUNT HOLDER: {{$comment->listing->user->name}}</h6>
                              <hr>
                              <h6>CONTACT: {{$comment->listing->user->phone}}</h6>
                              <hr>
                              <h6>USER NOTES: You can call for approval</h6>
                              </p>
                           </div>
                           <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <h5>PROOF OF PAYMENT</h5>
                              <p class="mb-0">   
                              <form action="/uploaddocs" method="post" enctype="multipart/form-data">
                                 {{ csrf_field() }}
                                 <div class="form-group">
                                    <input type="hidden" name="name" value="0" class="form-control">
                                 </div>
                                 <br />
                                 <input type="file" class="form-control" name="cvs[]" multiple />
                                 <br />
                                 <input type="submit" class="btn  btn-success" value="Upload" />
                              </form>
                              </p>
                           </div>
</div>
</div>



                                                                        </div>