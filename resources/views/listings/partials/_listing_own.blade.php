
@if ($listing->comments->count())

@else
  <div class="col-sm-12">
                        <h5 class="mb-3">Sale {{$listing->amount}} Coins</h5>


 <br>
                            <p>You are about to sale K{{$listing->amount}} on Auction</p><br>
                                    @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*24)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*24)))
@endif
                                    
                                        @if ($maturitydate->isPast())

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                         <button type="submit" class="btn btn-success mb-2">Sale on Auction</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@else

                                            <button type="submit" class="btn btn-primary mb-2">Available after {{$maturitydate->format('M d Y')}}</button>
                                       
@endif
@if (session()->has('impersonate'))

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                             <button type="submit" class="btn btn-success mb-2">Admin Sell</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@endif

@role('admin')

<form action="{{ route('listings.update', [$area, $listing]) }}" method="post">

                                         <input type="hidden" name="category_id" value="{{ $listing->category_id }}">
                                          <input type="hidden" name="amount" class="form-control" value="{{ $listing->amount }}">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-outline-success ul-btn-raised--v2 m-1 float-right" type="button">Admin Sell</button>

                                         {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                    </form>

@endrole





                                    </div>
                                   
                                </div>
                            </div>
                        </div>

          @endif
