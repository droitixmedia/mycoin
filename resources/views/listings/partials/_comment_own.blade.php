 <tbody>
                                             <tr>
                                               
                                                <td>
                                                   <h6 class="mb-1">Bid of R{{$comment->split}} for {{$comment->split}} Coins</h6>
                                                   <h6>{{$comment->created_at->format('M d Y')}}</h6>
                                                   <h6>{{$comment->listing->category->name}}</h6>
                                                </td>
                                                @if (!$comment->approvals->count())
                                                <td><a href="{{ route('comments.show', [$area,$comment]) }}" class="label theme-bg2 f-12 text-white">Pay</a></td>
                                                  @else
 <td><a href="#!" class="label theme-bg f-12 text-white">Payment Approved</a></td>

                                                  @endif
                                                    @if (!$comment->approvals->count())
                                                    <td>Not Paid.</td>

                                                    @else

                                                  <td>Coins will sell automatically on maturity date.</td>
                                                  @endif
                                               
                                             </tr>
                                            
                                          </tbody>