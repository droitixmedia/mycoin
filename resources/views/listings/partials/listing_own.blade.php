
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h3 class="card-title">R {{$listing->amount}} </h3>


                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>From</th>
                                                        <th>Contact</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       @if ($listing->comments->count())
                                    @foreach ($listing->comments as $comment)
                                                 <tr class="table-light">
                                                        <th scope="row">@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Approved</button>
                                                @else


                                                 <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
                                                    <input type="hidden" class="form-control" name="body" id="body" value="Approved">




                                            <button type="submit" class="btn btn-primary">Confirm</button>

                                                 {{ csrf_field() }}
                                                  </form>

                                              @endif
                                          </th>
                                                        <td>{{$comment->user->email}} </td>
                                                        <td>{{$comment->user->phone}}</td>
                                                        <td>{{$comment->split}}</td>
                                                    </tr>
                                                     @endforeach

                              @else
                                <h5>Selling</h5>

                              @endif
                                                     </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>




