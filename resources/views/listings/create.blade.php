@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
<div class="pcoded-main-container">
         <div class="pcoded-wrapper">
            <div class="pcoded-content">
               <div class="pcoded-inner-content">
                  <div class="page-header">
                     <div class="page-block">
                        <div class="row align-items-center">
                           <div class="col-md-12">


 <form action="{{ route('listings.store', [$area]) }}" method="post">
                                        <div class=" col-lg-12">
                              @include('listings.partials.forms._categories')
                                        </div>

                                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                            <label>Amount</label>
                                            <input type="text" name="amount" class="form-control">

                                              @if ($errors->has('amount'))
                                <span class="help-block">
                                    {{ $errors->first('amount') }}
                                </span>
                            @endif
                                        </div>
                                        <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-primary">Create</button>

                                         {{ csrf_field() }}
                                    </form>
</div>
</div>
</div>
</div>
</div>


@endsection
