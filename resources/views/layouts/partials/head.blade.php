<title>My Coin Auction - 2023</title>


<!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="My Coin Auction" />
<meta name="keywords" content="">
<meta name="author" content="Codedthemes" />

<link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="/assets/fonts/fontawesome/css/fontawesome-all.min.css">

<link rel="stylesheet" href="/assets/plugins/animation/css/animate.min.css">

<link rel="stylesheet" href="/assets/css/style.css">