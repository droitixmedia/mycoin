    <nav class="pcoded-navbar">
         <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
               <a href="{{url('/dash')}}" class="b-brand">
                  <div class="b-bg">
                     <i class="feather icon-trending-up"></i>
                  </div>
                  <span class="b-title">My Coin Auction</span>
               </a>
               <a class="mobile-menu" id="mobile-collapse" href="{{url('/dash')}}"><span></span></a>
            </div>
            <div class="navbar-content scroll-div">
               <ul class="nav pcoded-inner-navbar">
                  <li class="nav-item pcoded-menu-caption">
                     <label>Navigation</label>
                  </li>
                  <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{url('/dash')}}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Dashboard</span></a>
                     
                  </li>
                  @role('admin')
                   <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{url('/admin/users')}}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Users</span></a>
                     
                  </li>
                  <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{url('/admin/listings')}}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Listings</span></a>
                     
                  </li>
                   <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{url('/admin/impersonate')}}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Impersonate</span></a>
                     
                  </li>
                    <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{ route('listings.create', [$area]) }}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Create Bid</span></a>
                     
                  </li>

                  @endrole
                    @if (session()->has('impersonate'))
                     <li data-username="My Coin Auction" class="nav-item pcoded-hasmenu active pcoded-trigger">
                     <a href="{{ route('listings.create', [$area]) }}" class="nav-link"><span class="pcoded-micon"></span><span class="pcoded-mtext">Create Bid</span></a>
                     
                  </li>
                                       <li class="selected ">
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();" class="nk-menu-link">
                                         <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @endif
                  <li class="nav-item pcoded-menu-caption">
                     <label>Auction</label>
                  </li>
                  <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
                     <a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Auction Menu</span></a>
                     <ul class="pcoded-submenu">
                        <li class=""><a href="{{url('bidding')}}" class="">View Auction</a></li>
                        <li class=""><a href="{{ route('comments.published.index') }}" class="">Pay Bids</a></li>
                        <li class=""><a href="{{ route('listings.published.index', [$area]) }}" class="">Recieve Payments</a></li>
                       
                        <li class=""><a href="#" class="">BID Messages</a></li>
                        <li class=""><a href="{{ route('listings.unpublished.index', [$area]) }}" class="">Sale On Auction</a></li>
                        <li class=""><a href="{{url('bonus')}}" class="">Bonuses</a></li>
                        <li class=""><a href="{{url('affiliates')}}" class="">Affiliates</a></li>
                        <li class=""><a href="{{ route('listings.history.index', [$area]) }}" class="">Auction History</a></li>
                       
                     </ul>
                  </li>
              
                
                  <li class="nav-item pcoded-menu-caption">
                     <label>ADVANCED SETTINGS</label>
                  </li>
                  <li data-username="form elements advance componant validation masking wizard picker select" class="nav-item pcoded-hasmenu">
                     <a href="{{url('banking')}}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Banking Details</span></a>
                     
                  </li>
                  <li data-username="Table bootstrap datatable footable" class="nav-item pcoded-hasmenu">
                     <a href="{{url('security')}}" class="nav-link"><span class="pcoded-micon"><i class="feather icon-server"></i></span><span class="pcoded-mtext">Account Password</span></a>
                    
                  </li>
                  <li class="nav-item pcoded-menu-caption">
                     <label>AUTHENTICATION</label>
                  </li>
                   <li data-username="Table bootstrap datatable footable" class="nav-item pcoded-hasmenu">
                     <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="pcoded-micon"><i class="feather icon-power"></i></span><span class="pcoded-mtext">Sign Out</span></a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <header class="navbar pcoded-header navbar-expand-lg navbar-light">
         <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
            <a href="{{url('/dash')}}" class="b-brand">
               <div class="b-bg">
                  <i class="feather icon-trending-up"></i>
               </div>
               <span class="b-title">My Coin Auction</span>
            </a>
         </div>
         <a class="mobile-menu" id="mobile-header" href="#!">
         <i class="feather icon-more-horizontal"></i>
         </a>
         <div class="collapse navbar-collapse">
            
           
         </div>
      </header>
      <section class="header-user-list">
         <div class="h-list-header">
            <div class="input-group">
               <input type="text" id="search-friends" class="form-control" placeholder="Search Friend . . .">
            </div>
         </div>
         <div class="h-list-body">
            <a href="#!" class="h-close-text"><i class="feather icon-chevrons-right"></i></a>
            <div class="main-friend-cont scroll-div">
               <div class="main-friend-list">
                  <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . . </small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-3.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-4.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . . </small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-3.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-4.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . . </small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-3.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-4.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image ">
                        <div class="live-status">3</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Josephin Doe<small class="d-block text-c-green">Typing . . </small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Lary Doe<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-3.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Alice<small class="d-block text-c-green">online</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
                     <a class="media-left" href="#!">
                        <img class="media-object img-radius" src="../assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                        <div class="live-status">1</div>
                     </a>
                     <div class="media-body">
                        <h6 class="chat-header">Alia<small class="d-block text-muted">10 min ago</small></h6>
                     </div>
                  </div>
                  <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
                     <a class="media-left" href="#!"><img class="media-object img-radius" src="../assets/images/user/avatar-4.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body">
                        <h6 class="chat-header">Suzen<small class="d-block text-muted">15 min ago</small></h6>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="header-chat">
         <div class="h-list-header">
            <h6>Josephin Doe</h6>
            <a href="#!" class="h-back-user-list"><i class="feather icon-chevron-left"></i></a>
         </div>
         <div class="h-list-body">
            <div class="main-chat-cont scroll-div">
               <div class="main-friend-chat">
                  <div class="media chat-messages">
                     <a class="media-left photo-table" href="#!"><img class="media-object img-radius img-radius m-t-5" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body chat-menu-content">
                        <div class="">
                           <p class="chat-cont">hello Datta! Will you tell me something</p>
                           <p class="chat-cont">about yourself?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                     </div>
                  </div>
                  <div class="media chat-messages">
                     <div class="media-body chat-menu-reply">
                        <div class="">
                           <p class="chat-cont">Ohh! very nice</p>
                        </div>
                        <p class="chat-time">8:22 a.m.</p>
                     </div>
                  </div>
                  <div class="media chat-messages">
                     <a class="media-left photo-table" href="#!"><img class="media-object img-radius img-radius m-t-5" src="../assets/images/user/avatar-2.jpg" alt="Generic placeholder image"></a>
                     <div class="media-body chat-menu-content">
                        <div class="">
                           <p class="chat-cont">can you help me?</p>
                        </div>
                        <p class="chat-time">8:20 a.m.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="h-list-footer">
            <div class="input-group">
               <input type="file" class="chat-attach" style="display:none">
               <a href="#!" class="input-group-prepend btn btn-success btn-attach">
               <i class="feather icon-paperclip"></i>
               </a>
               <input type="text" name="h-chat-text" class="form-control h-send-chat" placeholder="Write hear . . ">
               <button type="submit" class="input-group-append btn-send btn btn-primary">
               <i class="feather icon-message-circle"></i>
               </button>
            </div>
         </div>
      </section>