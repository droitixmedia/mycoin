<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.head')

</head>
<body>
 <div class="loader-bg">
         
      </div>

   @include('layouts.partials.navigation')




       @yield('content')








</div>
      <script src="/assets/js/vendor-all.min.js"></script>
      <script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/assets/js/pcoded.min.js"></script>
     
      <script src="/assets/plugins/amchart/js/amcharts.js"></script>
      <script src="/assets/plugins/amchart/js/gauge.js"></script>
      <script src="/assets/plugins/amchart/js/serial.js"></script>
      <script src="/assets/plugins/amchart/js/light.js"></script>
      <script src="/assets/plugins/amchart/js/pie.min.js"></script>
      <script src="/assets/plugins/amchart/js/ammap.min.js"></script>
      <script src="/assets/plugins/amchart/js/usaLow.js"></script>
      <script src="/assets/plugins/amchart/js/radar.js"></script>
      <script src="/assets/plugins/amchart/js/worldLow.js"></script>
      <script src="/assets/js/pages/dashboard-ecommerce.js"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/6281f617b0d10b6f3e725c2b/1g35qijmq';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
