@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


 <div class="pcoded-main-container">
         <div class="pcoded-wrapper">
            <div class="pcoded-content">
               <div class="pcoded-inner-content">
                  <div class="page-header">
                     <div class="page-block">
                        <div class="row align-items-center">
                           <div class="col-md-12">
                              <div class="page-header-title">
                                 <h5 class="m-b-10">Welcome to My Coin Auction</h5>
                              </div>
                              <ul class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="feather icon-home"></i></a></li>
                                 <li class="breadcrumb-item"><a href="#!">home of {{Auth::user()->name}}</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="main-body">
                     <div class="page-wrapper">
                        <div class="row">
                    <h1>All Users {{$registeredusers}}</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                 <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col-sm-4">
                                                <div class="search-box mr-2 mb-2 d-inline-block">
                                                    <div class="position-relative">
                                                        <input type="text" class="form-control" placeholder="Search...">
                                                        <i class="bx bx-search-alt search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="text-sm-right">
                                                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 mr-2"><i class="mdi mdi-plus mr-1"></i> New Customers</button>
                                                </div>
                                            </div><!-- end col-->
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead>
                                                <tr><th>Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Bank Account</th>
                                                    <th class="text-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($users as $user)
                                                <tr><td>{{$user->id}}</td>
                                                    <td>
                                                        <h6 class="table-avatar">

                                                            {{$user->name}} {{$user->surname}}
                                                        </h6>
                                                    </td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->phone}}</td>
                                                    <td>{{$user->bank}} | {{$user->account}}</td>
                                                    <td class="text-right">
                                                        <div class="actions">
                                                            <a href="#" class="btn btn-sm bg-success-light mr-2">
                                                                <i class="fe fe-pencil"></i> Edit
                                                            </a>

                                                            <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $user->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete User"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.user.destroy', [$user->id])}}" method="post" id="listings-destroy-form-{{ $user->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                            </table>
                                            {{ $users->links() }}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
            </div><!-- Footer Start -->


@endsection
