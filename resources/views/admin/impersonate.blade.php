@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
<div class="pcoded-main-container">
         <div class="pcoded-wrapper">
            <div class="pcoded-content">
               <div class="pcoded-inner-content">
                  <div class="page-header">
                     <div class="page-block">
                        <div class="row align-items-center">
                           <div class="col-md-12">


 <form action="{{ route('admin.impersonate') }}" method="POST">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>

                            <button type="submit" class="btn btn-thm">Impersonate</button>
                            {{ csrf_field() }}
                        </form>
</div>
</div>
</div>
</div>
</div>


@endsection
