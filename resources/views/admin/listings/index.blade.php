@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


 <div class="pcoded-main-container">
         <div class="pcoded-wrapper">
            <div class="pcoded-content">
               <div class="pcoded-inner-content">
                  <div class="page-header">
                     <div class="page-block">
                        <div class="row align-items-center">
                           <div class="col-md-12">
                              <div class="page-header-title">
                                 <h5 class="m-b-10"> My Coin Auction</h5>
                              </div>
                              <ul class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="feather icon-home"></i></a></li>
                                 <li class="breadcrumb-item"><a href="#!">Bids</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="main-body">
                     <div class="page-wrapper">
                        <div class="row">

                 <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col-sm-4">
                                                <div class="search-box mr-2 mb-2 d-inline-block">
                                                    <div class="position-relative">
                                                        <input type="text" class="form-control" placeholder="Search...">
                                                        <i class="bx bx-search-alt search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="text-sm-right">
                                                    <button type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 mr-2"><i class="mdi mdi-plus mr-1"></i> Bids</button>
                                                </div>
                                            </div><!-- end col-->
                                        </div>


<div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead>
                                                    <tr>

                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Amount</th>
                                                        <th>Bank</th>
                                                        <th>Type</th>
                                                        <th>Creation Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      @foreach ($listings as $listing)




                                        <tr>
                                             @if ($listing->live())
                                             <td style="background-color: green">{{$listing->id}} ...Selling

                                            </td>
                                            <td style="background-color: green">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: green">{{$listing->user->email}}</td>
                                            <td style="background-color: green">{{$listing->user->phone}}</td>
                                            <td style="background-color: green">{{$listing->amount}}</td>
                                            <td style="background-color: green">{{$listing->user->bank}} | {{$listing->user->account}}</td>
                                             <td style="background-color: green">{{$listing->type}}</td>
                                            <td style="background-color: green">{{$listing->created_at}}</td>


                                            <td style="background-color: green"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>


                                    @else



                @if (is_null($listing->updated_at))
 @php($maturitydate = \Carbon\Carbon::parse($listing->created_at->addHours($listing->category->parent->color*18)))
@else
 @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->category->parent->color*18)))
@endif

 @if ($maturitydate->isPast())
        <td style="background-color: #de1a1a">{{$listing->id}}...Matured
             </td>
                                            <td style="background-color: #de1a1a">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: #de1a1a">{{$listing->user->email}}</td>
                                            <td style="background-color: #de1a1a">{{$listing->user->phone}}</td>
                                            <td style="background-color: #de1a1a">{{$listing->amount}}</td>
                                            <td style="background-color: #de1a1a">{{$listing->user->bank}} | {{$listing->user->account}}</td>
                                             <td style="background-color: green">{{$listing->type}}</td>
                                            <td style="background-color: #de1a1a">{{$listing->created_at}}</td>


                                            <td style="background-color: #de1a1a"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>

  @else
           <td style="background-color: gold">{{$listing->id}}...Maturing
             </td>
                                            <td style="background-color: gold">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: gold">{{$listing->user->email}}</td>
                                            <td style="background-color: gold">{{$listing->user->phone}}</td>
                                            <td style="background-color: gold">{{$listing->amount}}</td>
                                            <td style="background-color: gold">{{$listing->user->bank}} | {{$listing->user->account}}</td>
                                             <td style="background-color: green">{{$listing->type}}</td>
                                            <td style="background-color: gold">{{$listing->created_at}}</td>


                                            <td style="background-color: gold"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>

  @endif


                                    @endif
                                        </tr>

                                           @foreach ($listing->comments as $comment)
                                           <tr>
                                                <td>{{$comment->user->name}} {{$comment->user->surname}}</td>
                                                <td>{{$comment->user->email}}</td>
                                                <td>{{$comment->user->phone}}</td>
                                                <td>{{$comment->split}}</td>
                                                <td>{{$comment->created_at}}</td>
                                                <td>@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Paid</button>
                                                @else
                                                      <button type="submit" class="btn btn-warning">Not Paid</button>
                                                @endif

                                            </td>
                                            <td>
                                              <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $comment->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Reallocate Coins"><i class="fe fe-trash"></i>Reallocate</a></li>

                             <form action="{{route('admin.comment.destroy', [$comment->id])}}" method="post" id="comment-destroy-form-{{ $comment->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>


                                            </td>
                                             </tr>
                                           @endforeach


                                        @endforeach

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
            </div><!-- Footer Start -->


@endsection
