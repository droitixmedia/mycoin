<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>My Coin Auction</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
 <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
  <link href="/home/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/home/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="/home/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/home/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/home/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="/home/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="/home/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: TheEvent - v4.7.0
  * Template URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center ">
    <div class="container-fluid container-xxl d-flex align-items-center">

      <div id="logo" class="me-auto">
        <!-- Uncomment below if you prefer to use a text logo -->
        <!-- <h1><a href="index.html">The<span>Event</span></a></h1>-->
        <a href="{{url('/')}}" class="scrollto">My Coin Auction</a>
      </div>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          
          <li><a class="nav-link scrollto" href="#schedule">Schedule</a></li>
          <li><a class="nav-link scrollto" href="#faq">Faq</a></li>
          <li><a class="nav-link scrollto" href="#testimonial">Testimonial</a></li>
          
          <!-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
          <ul>
            <li><a href="#">Drop Down 1</a></li>
            <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
              <ul>
                <li><a href="#">Deep Drop Down 1</a></li>
                <li><a href="#">Deep Drop Down 2</a></li>
                <li><a href="#">Deep Drop Down 3</a></li>
                <li><a href="#">Deep Drop Down 4</a></li>
                <li><a href="#">Deep Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="#">Drop Down 2</a></li>
            <li><a href="#">Drop Down 3</a></li>
            <li><a href="#">Drop Down 4</a></li>
          </ul>
        </li> -->
        @guest
          <li><a class="nav-link scrollto" href="{{url('register')}}">Signup</a></li>
          @endguest
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
      @guest
      <a class="buy-tickets scrollto" href="{{url('login')}}">login</a>
      @else
<a class="buy-tickets scrollto" href="{{url('/dash')}}">Dashboard</a>
      @endguest

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
      <h1 class="mb-4 pb-0">THE AMAZINGLY IMPOSSIBLE<br><span>ONLINE</span> AUCTION</h1>
      <p class="mb-4 pb-0">That empowers you to be better, don`t be fooled by those who say "Impossible is nothing!"</p>
      <a href="https://youtu.be/EHOL3E-na24" class="glightbox play-btn mb-4"></a>
      <a href="#about" class="about-btn scrollto">Auction starts in</a>
       <a href="#about" class="about-btn scrollto">TBA</a>
    </div>
  </section><!-- End Hero Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-6">
            <h2>About The Auction</h2>
            <p>My Coin Auction is a fast growing online platform that enables you to participate in a virtual auction where you can bid and buy virtual coins called "My Coin", sale them and make a small fortune in either 5 days, 10 days or 20 days.</p>
          </div>
          <div class="col-lg-3">
            <h3>Where</h3>
            <p>In the comfort of your home.
In the palm of your hands.</p>
          </div>
          <div class="col-lg-3">
            <h3>When</h3>
            <p>Twice Everyday
<br>9 AM and 7 PM</p>
          </div>
        </div>
      </div>
    </section><!-- End About Section -->

   

   <section id="schedule" class="section-with-bg">
<div class="container wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
<div class="section-header">
<h2>Auction Schedule</h2>
<p>Here is what you need to do, to make a small fortune</p>
</div>
<ul class="nav nav-tabs" role="tablist">
<li class="nav-item">
<a class="nav-link active" href="#day-1" role="tab" data-toggle="tab">Day 1</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#day-2" role="tab" data-toggle="tab">Day 2</a>
</li>
<li class="nav-item">
<a class="nav-link" href="#day-3" role="tab" data-toggle="tab">Day 3</a>
</li>
</ul>
<h3 class="sub-heading">This Schedule only applies to registered members. To be part of this, create an account today.</h3>
<div class="tab-content row justify-content-center">

<div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
<div class="row schedule-item">
<div class="col-md-2"><time>09:20 AM <br> 19:20 PM</time></div>
<div class="col-md-10">
<h4>Login <span></span></h4>
<p>Be ready and be online to join others.</p>
</div>
</div>
<div class="row schedule-item">
<div class="col-md-2"><time>9  AM<br>7  PM</time></div>
<div class="col-md-10">
<h4>Access Auction<span></span></h4>
<p>Go to the auction and place your bid.</p>
</div>
</div>
<div class="row schedule-item">
<div class="col-md-2"><time>09:40 AM <br> 19:40 PM</time></div>
<div class="col-md-10">
<h4>Payment<span></span></h4>
<p>Complete payment as per your bids.</p>
</div>
</div>
</div>


<div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">
<div class="row schedule-item">
<div class="col-md-2"><time>09:00 AM <br> 17:00 PM</time></div>
<div class="col-md-10">
<h4>Check Account <span></span></h4>
<p>Ensure your paid bids are verified and your banking details are in order.</p>
</div>
</div>
</div>


<div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">
<div class="row schedule-item">
<div class="col-md-2"><time>09:20 AM <br> 19:20 PM</time></div>
<div class="col-md-10">
<h4>Login <span></span></h4>
<p>Login and place your coins on sale.</p>
</div>
</div>
<div class="row schedule-item">
<div class="col-md-2"><time>09:30 AM <br> 19:30 PM</time></div>
<div class="col-md-10">
<h4>Receive Payment<span></span></h4>
<p>Receive payment and approve it.</p>
</div>
</div>
</div>

</div>
</div>
</section>
<section id="about">
<div class="container">
<div class="row">
<div class="col-lg-12">
<center><h2>8492 members and counting</h2>
<p>Ask yourself, why My Coin Auction is hot like a tea kettle? </p></center>
</div>
</div>
</div>
</section>
  
   

  </main><!-- End #main -->

<footer id="footer">
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-6 footer-info">
<a href="#intro" class="scrollto">My Coin Auction</a>
<p>The most futuristic platform that enables you to grow daily, that empowers you to be better, don`t be fooled by those who say "Impossible is nothing!"</p>
<hr>
<p><a href="https://play.google.com/store/apps/details?id=com.mycoinauctionapp" target="_blank"><img src="https://i.pinimg.com/originals/71/42/62/714262829697e9749a4aa86b3c1f5538.png"></a></p>
<p hidden=""></p>
<br>
</div>
<div class="col-lg-3 col-md-6 footer-links">
<h4>Useful Links</h4>
<ul>
<li><i class="fas fa-angle-right"></i> <a href="#">Home</a></li>
<li><i class="fas fa-angle-right"></i> <a href="{{url('register')}}">Sign Up</a></li>
<li><i class="fas fa-angle-right"></i> <a href="{{url('login')}}">Login</a></li>
<li><i class="fas fa-angle-right"></i> <a href="privacy">Privacy Policy</a></li>
<li><i class="fas fa-angle-right"></i> <a href="terms">Terms And Conditions </a></li>
</ul>
</div>
<div class="col-lg-3 col-md-6 footer-links">
<h4>Useful Links</h4>
<ul>
<!--<li><i class="fas fa-angle-right"></i> <a href="password">Reset Password</a></li>-->
<!--<li><i class="fas fa-angle-right"></i> <script type="2e0e928d6bf62a8ea0d2b6c2-text/javascript" src="https://widget.supercounters.com/ssl/online_t.js"></script><script type="2e0e928d6bf62a8ea0d2b6c2-text/javascript">sc_online_t(1577685,"Users Online","#000000");</script><br><noscript></noscript></li>-->
<li><i class="fas fa-angle-right"></i> <a href="https://www.sslshopper.com/ssl-checker.html#hostname=www.mycoin.life" target="_blank">Our Security</a></li><strong></strong>
</ul>
</div>
<div class="col-lg-3 col-md-6 footer-contact">
<h4>Support</h4>
<p>
Our support is available during these hours <br>
<br>
08:00 AM<br>
21:00 PM<br>
</p>
</div>
</div>
</div>
</div>
<div class="container">
<div class="copyright">
Ver 1.92 © Copyright <strong>My Coin Auction</strong>. All Rights Reserved
</div>
<div class="credits">
</div>
</div>
</footer>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="/home/assets/vendor/aos/aos.js"></script>
  <script src="/home/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/home/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="/home/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="/home/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="/home/assets/js/main.js"></script>



</body>

</html>
