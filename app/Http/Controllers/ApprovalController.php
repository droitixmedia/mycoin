<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\Approval;
use openjobs\Comment;
use openjobs\Area;
use Auth;
use Session;

class ApprovalController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, $comment_id)
    {
       $comment = Comment::find($comment_id);




       $approval = new Approval();
       $approval->body = $request->body;

       $approval->user_id = auth()->user()->id;

       $approval->comment()->associate($comment);
       $approval->save();


     return back()->withSuccess('Approved');
}




     public function destroy(Area $area, Approval $approval)
    {
        $this->authorize('destroy', $approval);

        $approval->delete();

        return back()->withSuccess('Approval deleted.');
    }



}
