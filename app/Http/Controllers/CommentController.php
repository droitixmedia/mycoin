<?php

namespace openjobs\Http\Controllers;

use openjobs\Http\Requests\StoreCommentFormRequest;
use Illuminate\Http\Request;
use openjobs\Comment;
use openjobs\Listing;
use openjobs\Area;
use Auth;
use Session;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreCommentFormRequest $request, $listing_id)
    {
       $listing = Listing::find($listing_id);
                  $sum = 0;
    foreach($listing->comments as $comment){


              $sum += $comment->split;

              $sum;








     }
      if ($listing->amount-$sum < $request->split){
           return view('taken');

          }else{


       $comment = new Comment();
       $comment->body = $request->body;
       $comment->split = $request->split;
       $comment->user_id = auth()->user()->id;
       $comment->category_id = $request->category_id;
       $comment->listing()->associate($listing);
       $comment->save();


     return redirect()
            ->route('comments.published.index')
            ->with('success', 'Bid Secured');
 }
}


     public function destroy(Area $area, Comment $comment)
    {
        $this->authorize('destroy', $comment);

        $comment->delete();

        return back()->withSuccess('Comment was deleted.');
    }

    public function show(Request $request, Area $area, Comment $comment)
    {


        return view('comments.show', compact('comment'));
    }

     public function bitshow(Request $request, Area $area, Comment $comment)
    {


        return view('comments.bitshow', compact('comment'));
    }

    public function sell(Request $request, Comment $comment)
    {



        return view('sell', compact('comment'));
    }

}
