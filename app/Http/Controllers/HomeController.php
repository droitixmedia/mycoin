<?php

namespace openjobs\Http\Controllers;

use openjobs\{Area, Category, Listing, Comment};
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
      const INDEX_LIMIT = 10;

    public function dashboard(Area $area, Category $category, Listing $listing, Comment $comment, Request $request)
    {
         $listings = Listing::all()->where('live',true);

         $unpublishedcount = Auth::user()->listings()->where('live', false)->count();

        $user=Auth::user();
        $comments = $request->user()->comments()->paginate(10);


      

        return view('dashboard', compact('listings','unpublishedcount','comments','user'));
    }
    
    public function index(Area $area, Category $category, Listing $listing)
    {
          $listings = Listing::all()->where('live', true);

        


        return view('home', compact('listings'));
    }
    public function referral()
  {

  }

  public function referrer()
{
    return auth()->user()->referrer;
}

public function referrals()
{
    $referrals = auth()->user()->referrals()->paginate(20);

     return view('referrals', compact('referrals'));
}
public function bonuses()
{
    

     return view('pages.bonus');
}

}
