<?php

namespace openjobs\Http\Controllers;

use openjobs\Area;
use openjobs\Listing;
use openjobs\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latestListings = \App\Listing::orderBy('updated_at')->paginate(10);
        return view('admin.dashboard')->with(compact('latestListings'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function manageUsers()
    {
        $users = \App\User::orderBy('updated_at')->paginate(10);
        return view('admin.users.index')->with(compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyListing($id)
    {
        Listing::where('id', $id)->delete();
        session()->flash('message', 'Listing deleted!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUser($id)
    {
        User::where('id', $id)->delete();
        session()->flash('message', 'User deleted!');
        return redirect()->back();
    }
}
