<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;

use openjobs\Http\Controllers\Controller;
use Auth;

class FilePublishedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user=Auth::user();
        $files = $request->user()->files()->paginate(10);

        return view('user.files.published.index', compact('files','user'));
    }


}

