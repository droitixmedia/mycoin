<?php

namespace openjobs\Providers;

use Gate;
use openjobs\Permission;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class PermissionsServiceProvider extends ServiceProvider
{
 /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

       
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
